#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>

#define M_PI       3.14159265358979323846

using namespace std;

int nwm(int, int);
struct Point;
int min(int*, int);
int max(int*, int);
bool czyZbioryRowne(int*, int, int*, int);
bool czyTrojkat(int, int, int);
double rozwiazanieLiniowe(int, int);
int poleTrojkataZBokow(int, int, int);
double poleKola(int);
void point_sorting();

void display_matrix(int**, int, int);
int** transpose_matrix(int**, int, int);
void transpose_matrix_test();

const int NWD = 0, X = 1, Y = 2;
int* ext_euclidean(const int, const int);
void  ext_euclidean_test();
void display_array(int*, int);

void insertion_sort(int*, int  length);

double polygon_area(double*, int);

int main() {
	double input[] = { 0.72,2.28,
		2.66,4.71,
		5,3.5,
		3.63,2.52,
		4,1.6,
		1.9,1 };
	printf("area: %f", polygon_area(input, sizeof(input) / sizeof(double)));
	return 0;
}

/*
WARNING
input
x y x y x y
*/
double polygon_area(double* in, int length) {
	if (length % 2 != 0) {
		throw new exception("no chyba cie");
	}
	double area = 0;
	double x, x2, y, y2;
	x2 = in[0];
	y2 = in[1];
	for (int i = 2; i < length; i += 2) {
		x = in[i];
		y = in[i + 1];

		double height = (y + y2) / 2.0;
		double width = x2 - x;
		area += height * width;

		x2 = x;
		y2 = y;
	}
	return abs(area);
}

void insertion_sort(int* tab, int length) {
	int j;
	for (int i = 0;i < length;i++) {
		j = i;
		while (j > 0 && tab[j - 1] > tab[j]) {
			swap(tab[j], tab[j - 1]);
			j--;
		}
	}
}

void ext_euclidean_test() {
	int a, b;
	scanf("%d %d", &a, &b);
	int* wyniki = ext_euclidean(a, b);
	printf("nwd(%d,%d) = %d = %d*%d +(%d*%d)",
		a, b, wyniki[NWD], a, wyniki[X], b, wyniki[Y]);

	// zwalnia pamiec zajmowana przez nwd, x, y
	free(wyniki);
}

/*
Zwraca 3 inty:  nwd, x, y <- nwd(a,b) = a*x + b*y
**********************************************************************
i|czesc calk(q)| reszta(r)       | x               | y
0|             | 80              | 1               | 0
1|             | 5               | 0               | 1
2| 80 / 3 = 26 | 80 ? 3 � 26 = 2 | 1 ? 26 � 0 = 1  | 0 ? 26 � 1 = -26
3| 3 / 2 = 1   | 3 ? 1 � 2 = 1   | 0 ? 1 � 1 = {-1}| 1 ? 1 � ?26 = {27}
4| 2 / 1 = 2   | 2 ? 2 � 1 = 0   | 1 ? 2 � ?4 = 5  | ?26 ? 1 � 25 = ?51
*/
int* ext_euclidean(const int oryginalne_a, const int oryginalne_b) {
	int a = oryginalne_a;
	int b = oryginalne_b;

	int q; // ca�kowity dzielnik
	int r; // reszta z dzielenia a przez b

	// zmiana nastepuje w lewo (z prawej wskakuja kolejne dane)
	int x3 = 1, x2 = 0, x = 0;
	int y3 = 0, y2 = 1, y = 0;

	do {
		q = floor(a / b);
		r = a - q * b;

		// nwd(a,b) = nwd(b,a%b) gdzie % = dzielenie modulo
		a = b;
		b = r;

		x = x3 - q * x2;
		y = y3 - q * y2;

		// przesuniecia wszystkiego "do przodu"
		x3 = x2;
		x2 = x;
		y3 = y2;
		y2 = y;
	} while (r != 0);

	// koncowka petli przesunela je do tylu o 1
	x = x3;
	y = y3;

	int nwd = oryginalne_a * x + oryginalne_b * y;

	// malloc po to, by nie zniknely z pamieci
	int* wyniki = (int*)malloc(3 * sizeof(int));
	wyniki[NWD] = nwd;
	wyniki[X] = x;
	wyniki[Y] = y;
	return wyniki;
}

int nwd(int a, int b) {
	while (a != b) {
		if (a > b) {
			a -= b;
		}
		else if (b > a) {
			b -= a;
		}
	}
	// it doesnt matter -> a == b
	return a;
}
struct Point {
	string Name;
	int X;
	int Y;
	float distance() {
		return sqrt(X*X + Y*Y);
	}
};

int min(int* data, int length) {
	int min = data[0];
	for (int i = 1;i < length;i++) {
		if (data[i] < min) {
			min = data[i];
		}
	}
	return min;
}
int max(int* data, int length) {
	int max = data[0];
	for (int i = 1;i < length;i++) {
		if (data[i] > max) {
			max = data[i];
		}
	}
	return max;
}

/*
Zbiory MUSZ� mie� unikalne warto�ci !!! W ko�cu to zbiory
*/
bool czZbioryRowne(int* a, int aLength, int* b, int bLength) {
	if (aLength != bLength) {
		return false;
	}


	bool found = false;
	for (int i = 0; i < aLength; i++)
	{
		int searched = a[i];
		found = false;

		for (int j = 0; j < aLength; j++) {
			if (b[j] == searched) {
				found = true;
				break;
			}
		}
		// jezeli zbior b nie posiada jakiejkolwiek warto�ci ze zbioru b -> zbiory nie s� r�wne
		if (found == false) {
			return false;
		}
	}
	return found;
}

bool czyTrojkat(int a, int b, int c) {
	return (a < b + c) && (b < a + c) && (c < a + b);
}

double rozwiazanieLiniowe(int a, int b) {
	return b / a;
}

int poleTrojkataZBokow(int a, int b, int c) {
	int p = (a + b + c) / 2;	return sqrt(p*(p - a)*(p - b)*(p - c));
}

double  poleKola(int r) {
	return M_PI * r * r;
}

/* SPOJ - SORT1
*/
void point_sorting() {
	int t;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		int x;
		scanf("%d", &x);
		Point* p = new Point[x];
		for (int j = 0; j < x; x++)
		{
			scanf("%s %d %d", &p[j].Name, &p[j].X, &p[j].Y);
		}

		for (int j = 0; j < x; j++)
		{
			for (int k = 1; k < x; i++)
			{
				if (p[k].distance() < p[k - 1].distance()) {
					Point* tmp = &p[k];
					p[k] = p[k - 1];
					p[k - 1] = *tmp;
				}
			}
		}
		for (int j = 0; j < x; j++)
		{
			printf("%s %d %d", p[j].Name.c_str(), p[j].X, p[j].Y);
		}
	}

}
/*
//TODO
int extended_euclides(int a, int b) {
	if (b > a) {
		swap(a, b);
	}

	int q = floor(a / b);
	int r = a - q*b;
	int nwd = b;
	int
	x_2 = 1;
	x_1 = 0;
	int x = 1;

}
*/


int** transpose_matrix(int** matrix, int xLength, int yLength) {
	int** transposed = (int**)malloc(yLength * sizeof(int*));
	for (int i = 0;i < yLength;i++) {
		transposed[i] = (int*)malloc(xLength * sizeof(int));
	}


	for (int i = 0; i < xLength; i++) {
		for (int j = 0; j < yLength; j++)
		{
			transposed[j][i] = matrix[i][j];
		}
	}
	return transposed;
}

void display_matrix(int** matrix, int  xLength, int yLength) {
	for (int i = 0; i < xLength; i++)
	{
		for (int j = 0; j < yLength; j++)
		{
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
	}
}
void display_array(int* arr, int length) {
	for (int i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
}

void transpose_matrix_test() {
	int** m = (int**)malloc(3 * sizeof(int*));
	m[0] = (int*)malloc(3 * sizeof(int));
	m[1] = (int*)malloc(3 * sizeof(int));
	m[2] = (int*)malloc(3 * sizeof(int));
	m[0][0] = 1; m[0][1] = 2; m[0][2] = 3;
	m[1][0] = 4; m[1][1] = 5; m[1][2] = 6;
	m[2][0] = 7; m[2][1] = 8; m[2][2] = 9;

	printf("original matrix: \n");
	display_matrix(m, 3, 3);
	int** transposed = transpose_matrix(m, 3, 3);
	printf("transposed: \n");
	display_matrix(transposed, 3, 3);
}

